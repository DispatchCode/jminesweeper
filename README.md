# JMineSweeper #

JMineSweeper è l'ennesimo remake del celebre *Campo Minato*. Proprio 3 anni fa - aprile 2013 - decisi di riscriverlo migliorando una versione di qualche anno precedente.

Il software è scritto in Java e richiede quindi JRE installato per poter essere eseguito.

##Il Gioco##
Al momento sono disponibili 3 difficoltà: 

* Facile (griglia 9x9)

* Medio (griglia 16x16)

* Difficile (griglia 16x30)


Inoltre permette di salvare e caricare una partita. Cliccando con il tasto destro si indica la casellina con un punto interrogativo (quando si è dubbiosi sul suo contenuto o si vuole segnalare una mina).

Al contrario di buona parte degli altri remake e delle altre versioni diffuse, questa non effettua un controllo al momento del primo click per rilocare una possibile mina. 


## Screenshot ##

![2016-05-01_180117.png](https://bitbucket.org/repo/zkyeAp/images/2198990078-2016-05-01_180117.png)