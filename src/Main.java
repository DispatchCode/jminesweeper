/* ********************************************************
 * Descrizione: Main dell'applicazione.
 * Autore     : RootkitNeo
 *
 * Licenza    : GNU/GPL
 * ********************************************************
 */

class Main {
  public static void main(String[] args) {
    MineSweeperModel sm = new MineSweeperModel();
	MineSweeperView sv = new MineSweeperView();
	
	MineSweeperControl mc = new MineSweeperControl(sm, sv);
  }
}