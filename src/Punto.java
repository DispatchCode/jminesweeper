/* ********************************************************
 * Descrizione: coordinate della posizione all'interno della
 *              matrice che verranno passate al bottone
 * Autore     : RootkitNeo
 *
 * Licenza    : GNU/GPL
 * ********************************************************
 */
 

class Punto {
  private int r, c;
  
  Punto(int r, int c) {
    this.r = r;
    this.c = c;
  }
  
  int getRiga() {
    return r;
  }
  
  int getColonna() {
    return c;
  }
}