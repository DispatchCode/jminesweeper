import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;


/* ********************************************************
 * Descrizione: Classe per il salvataggio ed il caricamento
 *              delle partite.
 * Autore     : Marco 'RootkitNeo'
 *
 * Licenza    : GNU/GPL
 * ********************************************************
 */


class Partita {
  
  static boolean salva(String fileName, MineSweeperModel sm) {
    try {
      ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
      oos.writeObject(sm);
      oos.flush();
      oos.close();
    } catch(IOException e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }
  
  static MineSweeperModel carica(String fileName) {
    MineSweeperModel sm = null;
    try {      
      ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
      sm = (MineSweeperModel) ois.readObject();
      ois.close();
    } catch(Exception e) {
      e.printStackTrace();
      return null;
    }
    return sm;
  }
}