import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


/* ********************************************************
 * Descrizione: Bottone personalizzato, almeno in parte.
 *              in realta' uso un JButton con le dimensioni
 *              personalizzate.
 * Autore     : Marco 'RootkitNeo'
 *
 * Licenza    : GNU/GPL
 * ********************************************************
 */

class BottoneQuadrato extends JButton {
  private Punto punto;
  
  BottoneQuadrato(Punto punto) {
    super();

    this.punto = punto;
    
    setMargin(new Insets(0,0,0,0));
  }
  
  
  @Override
  public Insets getMargin() {
    return super.getMargin();
  }
  
  @Override
  public Dimension getPreferredSize() {
    return new Dimension(35,35);
  }
  
  @Override
  public Dimension getMinimumSize() {
    return getPreferredSize();
  }
  @Override
  public Dimension getMaximumSize() {
    return getPreferredSize();
  }

  
  Punto getPunto() {
    return punto;
  }
  
}