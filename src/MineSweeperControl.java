import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JMenuItem;
import javax.swing.*;


/* ********************************************************
 * Descrizione: Controller, interfaccia tra il Model e la GUI.
 * Autore     : Marco 'RootkitNeo'
 *
 * Licenza    : GNU/GPL
 * ********************************************************
 */

class MineSweeperControl implements ActionListener {
  //
  // ----------------------------------------------------------------------------
  private MineSweeperModel sm;               // Model dell'applicazione
  private MineSweeperView sv;                // View dell'applicazione
  private ActionListener al;                 // Ascoltatore sui pulsanti
  private MouseListener ml;
  // ----------------------------------------------------------------------------
  
  
  //
  // ----------------------------------------------------------------------------
  MineSweeperControl(MineSweeperModel sm, MineSweeperView sv) {
    this.sm = sm;
    this.sv = sv;
    
    // Avvia l'applicazione
    sv.init();
    al = this;
    ml = new MouseListenerButton();
    // Menu "File", "Difficolta", "Info"
    sv.creaMenu(new EventiMenu());
    sv.creaMenu(new EventiMenu());
    sv.creaMenu(new EventiMenu());
    sv.creaPannello(sm.getRighe(), sm.getColonne(), al, ml);
  }
  // -----------------------------------------------------------------------------
  
  // Gestione dei click sulle bottoni che rappresentano le celle
  // -----------------------------------------------------------------------------
  public void actionPerformed(ActionEvent ae) {
    BottoneQuadrato btn = (BottoneQuadrato) ae.getSource();
    
    Punto p = btn.getPunto();
    int res = sm.aggiornaCampo(p.getRiga(), p.getColonna());
    
    if(res == -1) {
      sm.mostraTutto();
      restituisciMappa();
      gestisciRispostaMessaggio(sv.messaggio("Hai Perso!"));
      return;
    }
    
    restituisciMappa();
    
    if(sm.controllaVittoria()) {
      gestisciRispostaMessaggio(sv.messaggio("Vittoria!"));
    }
    
  }
  // ------------------------------------------------------------------------------
  
  // Restituisce la mappatura delle celle visibili con il loro valore
  // ------------------------------------------------------------------------------
  private void restituisciMappa() {
    HashMap<Integer,Integer> mappa = sm.mappatura();
    
    Set<Map.Entry<Integer, Integer>> entrySet = mappa.entrySet();
    for(Map.Entry e : entrySet) {
      sv.aggiorna((Integer)e.getKey(),al, (Integer)e.getValue());
    }
  }
  // -------------------------------------------------------------------------------
  
  // Usato per gestire la risposta al momento della vittoria/sconfitta
  // -------------------------------------------------------------------------------
  private void gestisciRispostaMessaggio(String messaggio) {
    switch(messaggio) {
      case "Nuovo":
      sm.setDifficolta(sm.getDifficolta());
      sv.creaPannello(sm.getRighe(), sm.getColonne(), al, ml);
      break;
      case "Esci":
      sv.esci();
    }
  }
  // --------------------------------------------------------------------------------
  
  // Gestisco i click sui menu
  // (classe interna)
  // --------------------------------------------------------------------------------
  private class EventiMenu implements ActionListener {
  
    public void actionPerformed(ActionEvent ae) {
        Object btn = ae.getSource();
      
      // Menu Difficolta
      if(btn instanceof JRadioButtonMenuItem) {
        JRadioButtonMenuItem radioMenu = (JRadioButtonMenuItem) btn;
        
        if(radioMenu.getActionCommand().equals("9x9")) {
          sm.setDifficolta(0);
        }
        else if(radioMenu.getActionCommand().equals("16x16")) {
          sm.setDifficolta(1);
        }
        else if(radioMenu.getActionCommand().equals("16x30")) {
          sm.setDifficolta(2);
        }
      
        sv.creaPannello(sm.getRighe(), sm.getColonne(), al, ml);
      }
      // Menu File
      else {
        JMenuItem btnStandard = (JMenuItem) btn;
        
        if(btnStandard.getActionCommand().equals("Nuovo")) {
          sm.setDifficolta(sm.getDifficolta());
          sv.creaPannello(sm.getRighe(), sm.getColonne(), al, ml);
        }
        else if(btnStandard.getActionCommand().equals("Salva")) {
          if(Partita.salva("partita.txt", sm)) {
            sv.messaggioInfo("Salvataggio Riuscito!");
          }
          else {
            sv.messaggioInfo("Non e' stato possibile effettuare il salvataggio!");
          }
        }
        else if(btnStandard.getActionCommand().equals("Apri")) {
          sm = null;
          sm = Partita.carica("partita.txt");
          sv.creaPannello(sm.getRighe(), sm.getColonne(), al, ml);
          
          restituisciMappa();
        }
        else if(btnStandard.getActionCommand().equals("about")) {
          sv.messaggioInfo("Piccola e semplice implementazione del famoso Campo Minato.\n\nAutore: Marco 'DispatchCode' C.");
        }
      }
    }
  }
  // --------------------------------------------------------------------------------------------
  
  // Gestione click con il tasto destro del mouse sulle celle
  // (classe interna)
  // --------------------------------------------------------------------------------------------
  private class MouseListenerButton implements MouseListener {
    public void mouseEntered(MouseEvent me) {}
    public void mouseExited(MouseEvent me) {}
    public void mouseClicked(MouseEvent me) {}
    public void mousePressed(MouseEvent me) {}
    
    public void mouseReleased(MouseEvent me) {
      BottoneQuadrato bq = (BottoneQuadrato) me.getComponent();
      
      if(SwingUtilities.isRightMouseButton(me)) {
        if(!bq.getText().equals(sv.flag())) {
          sv.flagCasella(bq,true);
        } else {
          sv.flagCasella(bq,false);
        }
      }
    }
  }
  // -----------------------------------------------------------------------------------------------
}