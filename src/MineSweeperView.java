import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import javax.swing.*;
import java.util.ArrayList;
import java.io.File;
import javax.imageio.*;


/* ********************************************************
 * Descrizione: classe dedicata alla grafica. Si occupa
 *              solo di mostrare gli elementi
 * Autore     : RootkitNeo
 *
 * Licenza    : GNU/GPL
 * ********************************************************
 */


class MineSweeperView {
  //
  // -----------------------------------------------------------------------------
  private JPanel panel;                          // mostra gli elementi
  private JFrame finestra;
  //private ImageIcon image;                       
  
  private ArrayList<BottoneQuadrato> campo;      // Memorizza gli elementi che compongono il campo
  //private ImageIcon mina;
  private int righe, colonne;
  private String flag;
  // ------------------------------------------------------------------------------

  
  //
  // ------------------------------------------------------------------------------
  MineSweeperView() {
    /*
    try {
      image = new ImageIcon((Image) ImageIO.read(new File("cg.png")));
    } catch(Exception e) {}
    */
    
    flag = "?";
  }
  // -------------------------------------------------------------------------------
  
  
  // Crea il pannello con le relative dimensioni passate dal Controller
  // -------------------------------------------------------------------------------
  void creaPannello(int r, int c, ActionListener al, MouseListener ml) {
    this.righe = r;
    this.colonne = c;
    campo = null;
    campo = new ArrayList<BottoneQuadrato>(righe*colonne);
    
    // Si verifica solo quando � necessario un nuovo pannello;
    // premendo Nuovo, o cambiando difficolta'
    if(panel != null) {
      panel.removeAll();
      finestra.remove(panel);
    }
    
    panel = null;
    panel = new JPanel(new GridLayout(r,c));
    
    // Associo ogni bottone ai due ascoltatori
    for(int i=0; i<righe; i++) {
      for(int j=0; j<colonne; j++) {
        BottoneQuadrato bq = new BottoneQuadrato(new Punto(i,j));
        bq.addActionListener(al);
        bq.addMouseListener(ml);
        campo.add(bq);
        panel.add(bq);
      }
    }
    
    finestra.add(panel);
    finestra.pack();
  }
  // ----------------------------------------------------------------------------------
  
  // Crea il menu associandolo al Listener passato come parametro (dal Controller)
  // ----------------------------------------------------------------------------------
  void creaMenu(ActionListener al) {
    // Definizione dei menu
    // Menu file
    // ------------------------------------------
    JMenu file = new JMenu("File");
    JMenuItem nuovo = new JMenuItem("Nuovo");
    JMenuItem salva = new JMenuItem("Salva");
    JMenuItem apri  = new JMenuItem("Apri");
  
    // Difficolta'
    JMenu menu = new JMenu("Difficolta'");
    ButtonGroup bg = new ButtonGroup();
    JRadioButtonMenuItem facile    = new JRadioButtonMenuItem("Facile (9x9)");
    facile.setSelected(true);
    JRadioButtonMenuItem medio     = new JRadioButtonMenuItem("Medio (16x16)");
    JRadioButtonMenuItem difficile = new JRadioButtonMenuItem("Difficile (16x30");
    
    // About
    JMenu info = new JMenu("Info");
    JMenuItem about = new JMenuItem("Info");
    // -------------------------------------------
    
    // comandi generati all'invio dell'evento
    // --------------------------------------------
    // Menu File
    nuovo.setActionCommand("Nuovo");
    salva.setActionCommand("Salva");
    apri.setActionCommand("Apri");
    
    // Difficolta
    facile.setActionCommand("9x9");
    medio.setActionCommand("16x16");
    difficile.setActionCommand("16x30");
    
    // Informazioni
    about.setActionCommand("about");
    // --------------------------------------------
    
    // Ascoltatori sui menu
    // --------------------------------------------
    // File
    nuovo.addActionListener(al);
    salva.addActionListener(al);
    apri.addActionListener(al);
    
    // Difficolta
    facile.addActionListener(al);
    medio.addActionListener(al);
    difficile.addActionListener(al);
    
    // Informazioni
    about.addActionListener(al);
    // ---------------------------------------------
    
    // Creazione barra menu
    // ---------------------------------------------
    JMenuBar menuBar = new JMenuBar();
    
    // aggiunta componenti ai Menu
    // File
    file.add(nuovo);
    file.add(salva);
    file.add(apri);
    
    // Difficolta
    bg.add(facile);
    bg.add(medio);
    bg.add(difficile);
    menu.add(facile);
    menu.add(medio);
    menu.add(difficile);
    
    // Informazioni
    info.add(about);
    
    // Aggiunta menu alla barra ed alla finestra
    menuBar.add(file);
    menuBar.add(menu);
    menuBar.add(info);
    finestra.setJMenuBar(menuBar);
  }
  // -----------------------------------------------------------------
    

  // Aggiornamento di una casella con il rispettivo valore
  // -----------------------------------------------------------------
  void aggiorna(int index, ActionListener al, int valore) {
    BottoneQuadrato btn = campo.get(index);
    
    // Rimuovo il fastidioso focus che viene mostrato attorno
    // al contenuto del bottone
    btn.setFocusPainted(false);
    
    btn.removeActionListener(al);
    btn.setBackground(Color.WHITE);
    
    // Mina
    if(valore == -1) {
      //btn.setIcon(mina);
      btn.setBackground(Color.RED);
    }
    // casella vuota
    else if(valore == 0) {
      btn.setText("");
    }
    // casella numerata
    else {
      btn.setText(""+valore);
      btn.setFont(new Font("Helvetica", Font.BOLD, 16));
      btn.setForeground(coloreCorrispondente(valore));
    }
  }
  // ------------------------------------------------------------------
  
  // Utilizzata per segnare come "possibile mina" la casella
  // ------------------------------------------------------------------
  void flagCasella(BottoneQuadrato bq, boolean stato) {
    if(stato) {
      bq.setText(flag);
      bq.setFont(new Font("Helvetica", Font.BOLD, 16));
    } else {
      bq.setText("");
    }
  }
  // -------------------------------------------------------------------
  
  // Mostra il messaggio di informazioni passato (salva o carica)
  // -------------------------------------------------------------------
  void messaggioInfo(String messaggio) {
    JOptionPane.showMessageDialog(finestra, messaggio, "Partita Finita!", JOptionPane.INFORMATION_MESSAGE);
  }
  
  // Mostra il messaggio passato, all'interno di una finestra
  // con bottoni personalizzati
  String messaggio(String messaggio) {
    //Custom button text
    Object[] options = {"Nuovo", "Esci"};
    int n = JOptionPane.showOptionDialog(finestra,messaggio,"Scegli!", 
                                         JOptionPane.YES_NO_CANCEL_OPTION, 
                                         JOptionPane.QUESTION_MESSAGE,
                                         null, options, options[0]
                                        );
    return (String)options[n];
  }
  // --------------------------------------------------------------------
  
  // Restituise il colore corrispondente al valore
  private Color coloreCorrispondente(int valore) {
    switch(valore) {
      case 1:
      return new Color(0xF7,0xB9,0x44);
      case 2:
      return new Color(0xFA, 0x50, 0x00);
      case 3:
      return Color.RED;
      case 4:
      return Color.GREEN;
      case 5:
      return Color.BLUE;
      default:
      return Color.BLACK;
    }
  }
  // ---------------------------------------------------------------------
  
  
  void esci() {
    finestra.dispose();
  }
  
  String flag() {
    return flag;
  }
  
  // Avvio dell'applicazione; creazione finestra e piccoli settaggi
  // ----------------------------------------------------------------------
  private void creaGUI() {
    finestra = new JFrame("JMineSweeper | by Marco C.");
    finestra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    finestra.setResizable(false);
    finestra.setVisible(true);
  }
  
  void init() {
    try {
      SwingUtilities.invokeAndWait(new Runnable() {
        public void run() {
          creaGUI();
        }
      });
    } catch(Exception e) {}
  }
}