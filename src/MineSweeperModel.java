import java.io.Serializable;
import java.util.*;

/* ********************************************************
 * Descrizione: implementa la struttura del gioco stesso,
 *              fornendo un modello.
 * Autore     : RootkitNeo
 *
 * Licenza    : GNU/GPL
 * ********************************************************
 */


class MineSweeperModel implements Serializable {
  //
  // ---------------------------------------------------------------------------------------------
  private final int VUOTO_NASCOSTO =  0;
  private final int MINA           = -1;
  
  private int[][] campo;                       // tiene traccia delle celle vuote/numerate/con mine
  private boolean[][] mappa;                   // mappa gli le zone clickate e quelle non clickate
  private int righe, colonne, mine;
  private AttributiCampo ac;                   // Memorizza difficolta' e setta dimensioni matrici
  // ----------------------------------------------------------------------------------------------
  
  //
  // -------------------------------------------------------------
  MineSweeperModel() {
    ac      = new AttributiCampo(AttributiCampo.FACILE);
    
    campo   = new int[righe][colonne];
    mappa   = new boolean[righe][colonne];

    posizionaMine();
  }
  // ---------------------------------------------------------------
  
  int getRighe() {
    return campo.length;
  }
  int getColonne() {
    return campo[0].length;
  }
  
  // Posiziona le mine evitando sovrapposizioni
  // (non permette i duplicati)
  // ----------------------------------------------------------------
  private void posizionaMine() {
    Random riga    = new Random();
    Random colonna = new Random();
    int i=0;
    
    while(i < mine) {
      int ii = riga.nextInt(righe);
      int jj = colonna.nextInt(colonne);
      if(campo[ii][jj] != MINA) {
        campo[ii][jj] = MINA;
        contaAdiacenze(ii,jj);
        i++;
      }
    }    
    
    riga    = null;
    colonna = null;
  }
  // ------------------------------------------------------------------
  
  // Numero le caselle confinanti con una mina
  // ------------------------------------------------------------------
  private void contaAdiacenze(int i, int j) {
    // n,m-1
    if((j > 0) && (campo[i  ][j-1] != -1)) campo[i  ][j-1] += 1;
    // n-1,m
    if((i > 0) && (campo[i-1][j  ] != -1)) campo[i-1][j  ] += 1;
    // n-1,m+1
    if((i > 0) && (j < (colonne-1)) && (campo[i-1][j+1] != -1)) campo[i-1][j+1] += 1;
    // n-1,m-1
    if((i > 0) && (j > 0) && (campo[i-1][j-1] != -1)) campo[i-1][j-1] += 1;
    // n,m+1
    if((j < (colonne-1)) && (campo[i  ][j+1] != -1)) campo[i  ][j+1] += 1;
    // n+1,m
    if((i < (righe-1)) && (campo[i+1][j  ] != -1)) campo[i+1][j  ] += 1;
    // n+1,m-1
    if((i < (righe-1)) && (j > 0) && (campo[i+1][j-1] != -1)) campo[i+1][j-1] += 1;
    // n+1,m+1
    if((i < (righe-1)) && (j < (colonne-1)) && (campo[i+1][j+1] != -1)) campo[i+1][j+1] += 1;
  }
  // --------------------------------------------------------------------
  
  // Aggiorna il campo successivamente ad un click
  // --------------------------------------------------------------------
  int aggiornaCampo(int i, int j) {
    if(campo[i][j] == MINA) {
      mappa[i][j] = true;
      return MINA;
    }
    
    // espande le caselle senza mina (metodo ricorsivo)
    espandi(i,j);
    
    return 0;
  }
  // --------------------------------------------------------------------
  
  // Espande le caselle senza una mina; se trova una casella numerata
  // la espande e si ferma
  // --------------------------------------------------------------------
  private void espandi(int i, int j) {
    if((i<0 || j<0) || (i>righe-1 || j>colonne-1)) return;
    
    if(!mappa[i][j] && campo[i][j] == VUOTO_NASCOSTO) {
      mappa[i][j] = true;
    }
    else if(!mappa[i][j] && campo[i][j] > VUOTO_NASCOSTO) {
      mappa[i][j] = true;
      return;
    }
    else return;
    
    espandi(i,j+1);
    espandi(i+1,j);
    espandi(i-1,j);
    espandi(i,j-1);
    espandi(i+1, j+1);
    espandi(i-1, j-1);
    espandi(i-1, j+1);
    espandi(i+1, j-1);
  }
  // ----------------------------------------------------------------------
  
  
  // Restituisce la mappa che rappresenta
  // cella da mostrare -> valore contenuto
  // ------------------------------------------------------------------------
  HashMap<Integer, Integer> mappatura() {
    HashMap<Integer, Integer> associazioni = new HashMap<Integer, Integer>();
    int k=0;
    
    for(int i=0; i<righe; i++) {
      for(int j=0; j<colonne; j++) {
        if(mappa[i][j]) {
          associazioni.put(k, campo[i][j]);
        }
        k++;
      }
    }
    
    return associazioni;
  }
  // ---------------------------------------------------------------------------
 
  
  // Rende visibile l'intera matrice;
  // ---------------------------------------------------------------------------
  void mostraTutto() {
    for(int i=0; i<righe; i++) {
      for(int j=0; j<colonne; j++) {
        mappa[i][j] = true;
      }
    }
  }
  // ---------------------------------------------------------------------------
  
  
  // Metodo setter per la difficolta, usato per far ripartire il gioco
  // ---------------------------------------------------------------------------
  void setDifficolta(int difficolta) {
    ac.setDifficolta(difficolta);
    campo = null;
    mappa = null;
    
    campo = new int[righe][colonne];
    mappa = new boolean[righe][colonne];
    posizionaMine();
  }
  
  // ----------------------------------------------------------------------------
  
  // ----------------------------------------------------------------------------
  // restituisce true in caso di vittoria; false altrimenti
  boolean controllaVittoria() {
    for(int i=0; i<righe; i++) {
      for(int j=0; j<colonne; j++) {
        if(!mappa[i][j] && campo[i][j] != MINA) return false;
      }
    }
    return true;
  }
  // -----------------------------------------------------------------------------

  int getDifficolta() {
    return ac.getDifficolta();
  }
  
  boolean cellaVisibile(int i, int j) {
    return mappa[i][j];
  }
  
  private int[][] getCampo() {
    return campo;
  }
  
  private boolean[][] getMappa() {
    return mappa;
  }
  
  
  // Classe interna per gestire le difficlta'
  // ------------------------------------------------------------------------------
  private class AttributiCampo implements Serializable {
    public final static int FACILE    = 0;
    public final static int MEDIO     = 1;
    public final static int DIFFICILE = 2;
    private int difficolta;
    
    
    AttributiCampo(int difficolta) {
      setDifficolta(difficolta);
    }
    
    void setDifficolta(int difficolta) {
      this.difficolta = difficolta;
      switch(difficolta) {
        case 0:
        setFacile();
        break;
        case 1:
        setMedio();
        break;
        case 2:
        setDifficile();
        break;
        default:
        setFacile();
      }
    }
    
    private void setFacile() {
      righe   = 9;
      colonne =    9;
      mine    = 10;
    }
    
    private void setMedio() {
      righe   = 16;
      colonne = 16;
      mine    = 40;
    }
    
    private void setDifficile() {
      righe   = 16;
      colonne = 30;
      mine    = 99;
    }
    
    int getDifficolta() {
      return difficolta;
    }
  }
  // ---------------------------------------------------------------------------------
}
    